﻿using System;
using NUnit.Framework;

namespace StringCalculator1.Tests
{
    [TestFixture]
    public class TestAdd
    {
        [Test]
        public void Given_Empty_String_When_Adding_Then_Return_Zero()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("");

            //Assert
            Assert.AreEqual(actual, 0);
        }

        [Test]
        public void Given_One_Number_When_Adding_Then_Return_That_Number()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("11");

            //Asssert
            Assert.AreEqual(11, actual);
        }

        [Test]
        public void Given_Two_Numbers_When_Adding_Then_Return_Their_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("22,2");

            //Assert
            Assert.AreEqual(24, actual);
        }

        [Test]
        public void Given_Multiple_Numbers_When_Adding_Then_Return_Their_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("5,2,10001,6");

            //Assert
            Assert.AreEqual(13, actual);
        }

        [Test]
        public void Given_NewLine_Delimiter_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("5\n36,7");

            //Assert
            Assert.AreEqual(48, actual);
        }

        [Test]
        public void Given_Single_Character_Delimiter_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("//:\n32:147:63");

            //Assert
            Assert.AreEqual(242, actual);
        }

        [Test]
        public void Given_Negative_Numbers_When_Adding_Then_Throw_Exception()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = Assert.Throws<Exception>(() => calculator.Add("-63, -1"));

            //Assert
            Assert.AreEqual("Negatives not allowed -63, -1", actual.Message);
        }

        [Test]
        public void Given_Numbers_Bigger_Than_A_Thousand_When_Adding_Then_Ingore_Them()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("//:\n32:147:1001");

            //Assert
            Assert.AreEqual(179, actual);
        }

        [Test]
        public void Given_Single_Delimiter_With_Multiple_Characters_When_Adding_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("//:::::\n45:::::2:::::45:::::30");

            //Assert
            Assert.AreEqual(122, actual);
        }

        [Test]
        public void Given_Multiple_Delimiters_With_Single_Character_When_Adding_Then_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("//[:][%]\n45:2%45%30:67");

            //Assert
            Assert.AreEqual(189, actual);
        }

        [Test]
        public void Given_Multiple_Delimiters_With_Multiple_Characters_When_Adding_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = calculator.Add("//[:::::+][+++]\n45:::::+2:::::+45:::::+30+++67");

            //Assert
            Assert.AreEqual(189, actual);
        }

        [Test]
        public void Given_Multiple_Delimiters_Bad_string_With_Multiple_Characters_When_Adding_Return_Sum()
        {
            //Arrange
            var calculator = new Calculator();

            //Act
            var actual = Assert.Throws<Exception>(() => calculator.Add("//[$$][$@][$][@@]\n1$#2$$3@@5$@10"));

            //assert
            Assert.NotNull(actual);
        }
    }
}
