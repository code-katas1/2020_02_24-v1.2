﻿using System;
using System.Linq;

namespace StringCalculator1
{
    public class Calculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            string delimiterSeperatedNumber;

            if (numbers.StartsWith("//"))
            {
                string delimiter = GetDelimiters(numbers);
                delimiterSeperatedNumber = numbers.Substring(numbers.IndexOf("\n") + 1);
                delimiterSeperatedNumber = ConvertToDefaultDelimiters(delimiter, delimiterSeperatedNumber);

                IsBadString(delimiterSeperatedNumber);
            }
            else
            {
                delimiterSeperatedNumber = numbers.Replace("\n", ",");
            }

            int[] numbersArray = GetAllNumeric(delimiterSeperatedNumber);
            CheckForNegativeNumbers(numbersArray);

            return GetSum(numbersArray);
        }

        public string GetDelimiters(string numbers)
        {
            string multipleDelimiterStart = "//[", multipleDelimiterEnd = "]\n";

            if (numbers.StartsWith(multipleDelimiterStart) && numbers.Contains(multipleDelimiterEnd))
            {
                string firstLine = numbers.Substring(numbers.IndexOf(multipleDelimiterStart) + 2, numbers.IndexOf(multipleDelimiterEnd) - 1);
                firstLine = firstLine.Replace("][", ",");

                return firstLine.Replace("[", "").Replace("]", "");
            }

            return numbers.Substring(numbers.IndexOf("//") + 2, numbers.IndexOf("\n") - 2);
        }

        public string ConvertToDefaultDelimiters(string delimiter, string delimiterSeperatedNumber)
        {
            string[] delimiterArray = delimiter.Split(',');

            for (int i = 0; i < delimiterArray.Length; i++)
            {
                delimiterSeperatedNumber = delimiterSeperatedNumber.Replace(delimiterArray[i], ",");
            }

            return delimiterSeperatedNumber;
        }

        public void IsBadString(string delimiterSeperatedNumber)
        {
            for (int i = 0; i < delimiterSeperatedNumber.Length; i++)
            {
                if (delimiterSeperatedNumber[i] != '-' && !char.IsDigit(delimiterSeperatedNumber[i]) && delimiterSeperatedNumber[i] != ',')
                {
                    throw new Exception("Bad string");
                }
            }
        }

        public int[] GetAllNumeric(string delimiterSeperatedNumbers)
        {
            return delimiterSeperatedNumbers.Split(',').Select(int.Parse).ToArray();
        }

        public void CheckForNegativeNumbers(int[] numbersArray)
        {
            string negativeNumbers = null;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 0)
                {
                    negativeNumbers += ", " + numbersArray[i];
                }
            }

            if (!string.IsNullOrEmpty(negativeNumbers))
            {
                negativeNumbers = negativeNumbers.Substring(1).Trim();
                throw new Exception("Negatives not allowed " + negativeNumbers);
            }
        }

        public int GetSum(int[] numbersArray)
        {
            int sum = 0;

            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 1001)
                {
                    sum += numbersArray[i];
                }
            }

            return sum;
        }

    }
}
